package scenarios

import io.gatling.core.Predef._
import requests.PostNewComputer.{postRequest, buildingParameterizedPostRequest}

object NewComputer {
  val postNewComputerScenario = scenario("Add new computer").exec(postRequest)

  val computerNamesFeeder = csv("./src/test/resources/data/computer_names.csv").circular
  val computerIntroDatesFeeder = csv("./src/test/resources/data/computer_introduction_dates.csv").circular
  val postNewComputersScenario = scenario("Add new computers")
    .feed(computerNamesFeeder)
    .feed(computerIntroDatesFeeder)
    .exec(buildingParameterizedPostRequest("${computerName}", "${computerIntroDate}", "", "5"))
}
