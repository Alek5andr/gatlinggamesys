package simulation

import io.gatling.core.Predef.Simulation
import io.gatling.core.Predef._

import config.TestConfig.{httpProtocol, users, maxResponseTime}
import scenarios.NewComputer.{postNewComputerScenario, postNewComputersScenario}

class AddNewComputer extends Simulation {
  before {
    println("=== Simulation is about to start! ===")
  }

  after {
    println("=== Simulation is finished! ===")
  }

  val multipleScenarios = List(
    postNewComputerScenario.pause(5).inject(
      rampUsersPerSec(users) to 20 during 10
    ),
    postNewComputersScenario.pause(3).inject(
      atOnceUsers(users)
    )
  )

  setUp(multipleScenarios)
    .protocols(httpProtocol)
    .assertions(
      global.responseTime.max.lte(maxResponseTime)
    )

}
