package config

import scala.sys.SystemProperties

object SystemPropertiesEnhancer {
  val systemProperties = new SystemProperties

  def getAsStringOrElse(property: String, default: String): String = {
    systemProperties.getOrElse(property, default)
  }

  def getAsIntOrElse(property: String, default: Int): Int = {
    systemProperties.getOrElse(property, default).toString.toInt
  }
}
