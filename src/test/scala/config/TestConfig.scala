package config

import config.SystemPropertiesEnhancer._
import io.gatling.core.Predef.{BlackList, WhiteList}
import io.gatling.http.Predef.http
import io.gatling.core.Predef._

object TestConfig {
  val baseUrl = getAsStringOrElse("baseUrl", "http://computer-database.gatling.io/computers")
  val httpProtocol = http
    .baseUrl(baseUrl)
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""), WhiteList())
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.9")
    .doNotTrackHeader("1")

  val users = getAsIntOrElse("user", 6)
  val maxResponseTime = getAsIntOrElse("maxResponseTime", 120)
}
