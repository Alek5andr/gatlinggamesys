package requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

object PostNewComputer {
  def buildingParameterizedPostRequest(name: String, introDate: String, discontinuedDate: String, company: String):  HttpRequestBuilder = {
    http("POST new computers")
      .post("")
      .formParam("name", name)
      .formParam("introduced", introDate)
      .formParam("discontinued", discontinuedDate)
      .formParam("company", company)
      .check(status.is(200))
  }

  val parametersMap = Map(
    "name" -> "Cactus",
    "introduced" -> "1971-01-01",
    "discontinued" -> "",
    "company" -> "5"
  )

  val postRequest = http("POST new computer")
    .post("")
    .formParamMap(parametersMap)
    .check(status.is(200))
}
