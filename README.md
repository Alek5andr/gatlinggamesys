# Task 1
Answers to questions from "Task 1" are in file _assignment/Gamesys_task_1.pdf_

# Task 2
* Clone this project as **Maven** project
* Import Maven dependencies
* Execute Maven command `mvn verify` being in project's root directory
    * If file "pom.xml" was somehow scattered around, then execute `mvn verify -f pom.xml_FILE_LOCATION`
* Evaluate
    * If not enough code for evaluation, check other projects outside this one